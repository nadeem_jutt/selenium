from locale import currency
from posixpath import curdir
from tabnanny import check
from booking.booking import Booking

try:
    with Booking() as bot:
        bot.land_first_page()
        # print('Exiting ...')

        bot.change_currency(currency='USD')
        bot.select_place_to_go('New York')
        bot.select_dates(check_in_date='2022-02-20',
                        check_out_date='2022-02-22')
        bot.select_adults(10)
        bot.click_search()
        bot.apply_filtrations()
        bot.report_results()

except Exception as e:
    print('There is a problem running this program from command line interface')