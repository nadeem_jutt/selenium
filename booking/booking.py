from lib2to3.pgen2 import driver
from re import search
from select import select
from ssl import Options
import booking.constants as const
import os
from selenium import webdriver
from booking.booking_filtration import BookingFiltration
# from booking.booking_report import BookingReport 

import time

from selenium.webdriver.common.keys import Keys

# PATH = "C:\Program Files (x86)\chromedriver.exe"
# driver = webdriver.Chrome(PATH)

class Booking(webdriver.Chrome):
    def __init__(self, driver_path =r";C:\Program Files (x86)"):
        # print(driver_path)
        # exit()
        self.driver_path = driver_path
        
        os.environ['PATH'] += self.driver_path
        # print(os.environ['PATH'])
        # exit()
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        super(Booking, self).__init__(options=options)

    # def __exit__(self, exc_type, exc_val, exxc_tb:)
    #     self.quit()

    def land_first_page(self):
        self.get(const.BASE_URL)

    def change_currency(self, currency=None):
        print('change currency started')
        currency_element = self.find_element_by_css_selector(
            'button[data-tooltip-text="Choose your currency"]'
        )
        currency_element.click()
        self.implicitly_wait(1000) # seconds
        print('change currency done')

        selected_currency_element = self.find_element_by_css_selector(
            f'a[data-modal-header-async-url-param*="selected_currency={currency}"]'
        )
        selected_currency_element.click()

    def select_place_to_go(self, place_to_go):
        search_field = self.find_element_by_id('ss')
        search_field.click()
        search_field.send_keys(place_to_go)

        first_result = self.find_element_by_css_selector(
            'li[data-i="0"]'
        )
        first_result.click()
        print('place select done')

    def select_dates(self, check_in_date, check_out_date):
        check_in_element = self.find_element_by_css_selector(
            f'td[data-date="{check_in_date}"]'
        )
        check_in_element.click()

        check_out_element = self.find_element_by_css_selector(
            f'td[data-date="{check_out_date}"]'
        )
        check_out_element.click()
        print('date select done')

    def select_adults(self, count=1):
        selection_element = self.find_element_by_id('xp__guests__toggle')
        selection_element.click()

        while True:
            decrease_adults_element = self.find_element_by_css_selector(
                'button[aria-label="Decrease number of Adults"]'
            )
            decrease_adults_element.click()
            #If value of adults reaches 1, then we should get out
            #of the while loop
            adults_value_element = self.find_element_by_id('group_adults')
            adults_value = adults_value_element.get_attribute('value') # Should give back adult count

            if int(adults_value) == 1:  # "get_attribute" return string value
                break
        print('adult count set to 1 . done')

        Increase_button_element = self.find_element_by_css_selector(
                'button[aria-label="Increase number of Adults"]'
        )

        for _ in range(count - 1):
            Increase_button_element.click()

    def click_search(self):
        search_button = self.find_element_by_css_selector(
            'button[type="submit"]'
        )
        search_button.click()
        print('search button . done')

    def apply_filtrations(self):
        filtration = BookingFiltration(driver=self)
        filtration.apply_star_rating(3, 4, 5)

        filtration.sort_price_lowest_first()

    def report_results(self):

        count = self.find_element_by_xpath(
            "//*[contains(@data-testid,'property-card')]"
        )
        print(count)
        time.sleep(100)
        # hotel_boxes = self.find_element_by_xpath(
        #     "//*[contains(@data-component,'arp-properties-list')]"
        # ).find_element_by_xpath(
        #     "//*[contains(@data-testid,'property-card')]"
        # )
        # return hotel_boxes
        




